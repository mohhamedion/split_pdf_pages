<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Storage;
use Smalot\PdfParser\Parser;
use \setasign\Fpdi\Fpdi as FPDI;

class PdfController extends Controller
{


    public function index()
    {

        $file = request()->file()['pdf'];
        $parser = new Parser();

        $pdf_get_pages = new Fpdi();
        $countPages = $pdf_get_pages->setSourceFile($file->getRealPath());


        for ($pageNumber = 1; $pageNumber < $countPages; $pageNumber++) {

            // get first page
            $pdf = new Fpdi();
            $pdf->setSourceFile($file->getRealPath());
            $pdf->AddPage();
            $tplIdx = $pdf->importPage($pageNumber);
            $pdf->useImportedPage($tplIdx, 10, 10, 200);
            $stringContent = $pdf->Output('S');
            //PS: после вызова $pdf->Output('S') документ закрывается, поэтому для добавления страницы нужно будет создать новый объект класса Fpdi


            // parser page to string
            $pages = $parser->parseContent($stringContent);
            $pageText = $pages->getText();

            // номер ЛС или false
            $accountNumber = $this->get_number($pageText);
            $pdfContainFirstPage = (bool)$accountNumber;
            $pdfContainSecondPage = strpos($pageText, 'администрация ООО "Развитие-21"');

            // если пдф содержит и первую и вторую страницу
            if ($pdfContainFirstPage &&  $pdfContainSecondPage) {

                $month  = $this->getMonthDate($pageText);
                // нельзя скачать с помощью $pdf->Output('D','file.pdf') потому-что не дает указать название папки.
                Storage::disk('system')->put("{$month}/".$accountNumber.'.pdf', $stringContent);

            }else if($pdfContainFirstPage){

                //создать новый пдф
                $pdf = new Fpdi();
                $pdf->setSourceFile($file->getRealPath());

                //добавить первую страницу

                $pdf->AddPage();
                $firstPage = $pdf->importPage($pageNumber );
                $pdf->useImportedPage($firstPage, 10, 10, 200);

                //добавить вторую страницу

                $pdf->AddPage();
                $secondPage = $pdf->importPage($pageNumber + 1);
                $pdf->useImportedPage($secondPage, 10, 10, 200);

                $stringContent = $pdf->Output('S');

                $month  = $this->getMonthDate($pageText);
                // нельзя скачать с помощью $pdf->Output('D','file.pdf') потому-что не дает указать название папки.
                Storage::disk('system')->put("{$month}/".$accountNumber.'.pdf', $stringContent);
            }


        }


//
//        //download
//        $pdf->Output('I');
//
//        $pdf->Output('D',$accountNumber.'.pdf');
    }


    function getMonthDate($pageText)
    {
        $months = array(
            '01' => 'Январь',
            '02' => 'Февраль',
            '03' => 'Март',
            '04' => 'Апрель',
            '05' => 'Май',
            '06' => 'Июнь',
            '07' => 'Июль',
            '08' => 'Август',
            '09' => 'Сентябрь',
            '10' =>  'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь');
        foreach ($months as $key => $month) {
            if (strpos($pageText, $month)) {
                return $key;
            }
        }
        return false;
    }


    function get_number($pageText)
    {
        $startFrom = strpos($pageText, 'л/с №');

        if (!$startFrom) {
            return false;
        }

        $string = substr($pageText, $startFrom);
        $arrays = explode(' ', $string);
        return preg_replace("/[^0-9]/", "", $arrays[1]);
    }


}
